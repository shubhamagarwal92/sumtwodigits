# Here we define the basic architecture of the LeNet5 model 
# that would be used. 
# Dropout not added for convolutional layers as they have the 
# resistence themselves against overfitting
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense

def lenet5Model(inputShape,classes,kernel1,kernel2,poolSize,
				layer1Filters,layer2Filters,weightDir=None):
	model = Sequential()
	# Conv Layer1
	model.add(Convolution2D(layer1Filters, kernel1[0], kernel1[1],border_mode='valid',
							input_shape=inputShape))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=poolSize))
	# Conv Layer2
	model.add(Convolution2D(layer2Filters, kernel2[0],kernel2[1],border_mode='valid'))
	model.add(Activation("relu"))
	model.add(MaxPooling2D(pool_size=poolSize))
	# Fully connected layer
	model.add(Dropout(0.5))
	model.add(Flatten())
	model.add(Dense(784))
	model.add(Activation("relu"))
	# Dropout as natural regularizer in Deep Neural Networks
	model.add(Dropout(0.5))
	model.add(Dense(classes))
	model.add(Activation("softmax"))
	# Pre-loaded weights
	if weightDir is not None:
		model.load_weights(weightDir)
	return model