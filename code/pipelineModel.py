from dataImport import *
import matplotlib.pyplot as plt
import keras
from lenet5 import *
from keras.optimizers import SGD
from keras import backend as K
from keras.layers import Merge, Dense
from sklearn import metrics
from sklearn.metrics import classification_report

K.set_image_dim_ordering('th')

kernel1 = (5,5)
kernel2 = (5,5)
poolSize = (2,2)
layer1Filters = 20
layer2Filters = 50
imageSize = (1,28,28)
weightDir = None
batchSize = 32
epoch = 50
# weightDir = codeDir[:-4] + 'models/mnist_lenet.h5'
model1 = lenet5Model(imageSize,10,kernel1,kernel2,poolSize,
					layer1Filters,layer2Filters,weightDir)
model2 = lenet5Model(imageSize,10,kernel1,kernel2,poolSize,
					layer1Filters,layer2Filters,weightDir)

arithmeticModel = Sequential()
arithmeticModel.add(Merge([model1,model2], mode='concat'))
arithmeticModel.add(Dense(100, activation='relu'))
arithmeticModel.add(Dropout(0.5))
arithmeticModel.add(Dense(nb_classes, activation='softmax'))

arithmeticModel.compile(loss='categorical_crossentropy',
                optimizer='adadelta',
                metrics=['accuracy'])


arithmeticModel.fit([trainX[:,[0],:,:], trainX[:,[1],:,:]], trainY,
            batch_size=batchSize, nb_epoch=epoch,
            validation_data=([valX[:,[0],:,:], valX[:,[1],:,:]], valY))
predY = arithmeticModel.predict([testX[:,[0],:,:], testX[:,[1],:,:]])
# Convert one-hot to index
predY = np.argmax(predY, axis=1)
from sklearn.metrics import classification_report
print(classification_report(testY, predY))
print(metrics.confusion_matrix(testY, predY))
print(metrics.accuracy_score(testY, predY))

# score = arithmeticModel.evaluate([testX[:,[0],:,:], testX[:,[1],:,:]], testY, verbose=1)
# print('Test score:', score[0])
# print('Test accuracy:', score[1])
