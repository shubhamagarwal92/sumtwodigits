# This script loads data. 
# No pre-processing required
import os 
import h5py
from keras.utils import np_utils
import numpy as np
import collections

# from keras.preprocessing.image import ImageDataGenerator
np.random.seed(0)

nb_classes = 19
codeDir = os.path.dirname(os.path.realpath(__file__))
# print codeDir
dataDir = codeDir[:-4] + 'dataset_intern/'
# print dataDir
trainPath = dataDir + 'training_set.hdf5'
valPath = dataDir + 'validation_set.hdf5'
testPath = dataDir + 'testing_set.hdf5'

# If string values instead of integers
# from sklearn.preprocessing import LabelEncoder
# Encode once using sklearn encoder for class labels
# encoder = LabelEncoder()
# encoder.fit(trainY)
# trainY = encoder.transform(trainY)

def load_data(dataPath,flag):
	dataFile = h5py.File(dataPath, 'r')
	dataX = dataFile["input_samples"]
	dataY = dataFile["label"]
	# print collections.Counter(dataY)
	if flag==1:
		dataY = np_utils.to_categorical(dataY)
	dataX = np.array(dataX)
	dataY = np.array(dataY)
	return dataX, dataY

trainX, trainY = load_data(trainPath,flag=1)
valX, valY = load_data(valPath,flag=1)
testX, testY = load_data(testPath,flag=0)
