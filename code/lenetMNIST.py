# This is the main script that trains the LeNet5 model
# Calls lenet5.py and dataImport.py
# The structure is such that we have all three files in "Code" dir
# and the dataset in "dataset_intern". The model would be saved in "models" dir 

from dataImport import *
import matplotlib.pyplot as plt
import keras
from lenet5 import *
from keras.optimizers import SGD
from keras import backend as K
from sklearn import metrics
from sklearn.metrics import classification_report
# Callback
from keras.callbacks import Callback
import pylab as pl
import matplotlib.cm as cm

class TrainingHistory(Callback):
    def on_train_begin(self, logs={}):
        self.losses = []
    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))

K.set_image_dim_ordering('th')

# plt.figure()
# plt.subplot(2,1,1)
# plt.imshow(trainX[0][0],cmap=plt.get_cmap('gray'))
# plt.subplot(2,1,1)
# plt.imshow(trainX[0][1],cmap=plt.get_cmap('gray'))
# plt.show()
# print trainY[0]
# print trainX.shape
kernel1 = (5,5)
kernel2 = (5,5)
poolSize = (2,2)
layer1Filters = 20
layer2Filters = 50
imageSize = trainX[0].shape
weightDir = None
batchSize = 32
epoch = 60
# print imageSize
# print poolSize
# print kernel1
# print kernel1[0]


model = lenet5Model(imageSize,nb_classes,kernel1,kernel2,poolSize,
					layer1Filters,layer2Filters,weightDir)
model.compile(loss='categorical_crossentropy',
              optimizer='adadelta',
              metrics=['accuracy'])
# model.compile(loss='categorical_crossentropy',
#               optimizer='sgd',
#               metrics=['accuracy'])
# model.compile(loss='mean_squared_error',
#               optimizer='sgd',
#               metrics=['accuracy'])
# print model.summary()

history = TrainingHistory()
model.fit(trainX, 
		  trainY, 
		  batch_size=batchSize, 
		  nb_epoch=epoch,
          verbose=1, 
          validation_data=(valX, valY),
          callbacks=[history])
# # Plot weight layers
# print len(model.layers)
# weights = model.layers[3].get_weights()
# # print weights
# pl.imshow(weights[0][0][0],cmap=cm.binary)
# pl.show()

# # Plot history of error and losses
# plt.figure(figsize=(6, 3))
# plt.plot(history.losses)
# plt.ylabel('error')
# plt.xlabel('iteration')
# plt.title('training error')
# plt.show()

predY = model.predict(testX)
# Convert one-hot to index
predY = np.argmax(predY, axis=1)
from sklearn.metrics import classification_report
print(classification_report(testY, predY))
print(metrics.confusion_matrix(testY, predY))
print(metrics.accuracy_score(testY, predY))

# score = model.evaluate(testX, testY, verbose=1)
# # print('Test score:', score[0])
# print('Test accuracy:', score[1])

# print model.summary()

###### Save model ###### 
modelDir = codeDir[:-4] + 'models/'
model_json = model.to_json()
modelFile = modelDir + "model.json"
with open(modelFile, "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights(modelDir+"mnist_lenet.h5")
print("Saved model to disk")

