# Refer
# http://machinelearningmastery.com/grid-search-hyperparameters-deep-learning-models-python-keras/

from dataImport import *
import matplotlib.pyplot as plt
import keras
from lenet5 import *
from keras.optimizers import SGD
from keras import backend as K
from sklearn import metrics
from sklearn.metrics import classification_report
# Callback
# from keras.callbacks import Callback
# import pylab as pl
# import matplotlib.cm as cm
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV
#from sklearn.grid_search import GridSearchCV

K.set_image_dim_ordering('th')

kernel1 = (5,5)
kernel2 = (5,5)
poolSize = (2,2)
layer1Filters = 20
layer2Filters = 50
imageSize = trainX[0].shape
weightDir = None
# batchSize = 32
# epoch = 50
#lenetModel = lenet5Model(imageSize,nb_classes,kernel1,kernel2,poolSize,
#					layer1Filters,layer2Filters,weightDir)

def lenetModel():
	inputShape = imageSize
	classes = nb_classes
	model = Sequential()
	# Conv Layer1
	model.add(Convolution2D(layer1Filters, kernel1[0], kernel1[1],border_mode='valid',
							input_shape=inputShape))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=poolSize))
	# Conv Layer2
	model.add(Convolution2D(layer2Filters, kernel2[0],kernel2[1],border_mode='valid'))
	model.add(Activation("relu"))
	model.add(MaxPooling2D(pool_size=poolSize))
	# Fully connected layer
	model.add(Dropout(0.5))
	model.add(Flatten())
	model.add(Dense(784))
	model.add(Activation("relu"))
	# Dropout as natural regularizer in Deep Neural Networks
	# model.add(Dropout(0.5))
	# model.add(Dense(20))
	# model.add(Activation("relu"))
	model.add(Dropout(0.5))
	model.add(Dense(classes))
	model.add(Activation("softmax"))
	# Pre-loaded weights
	if weightDir is not None:
		model.load_weights(weightDir)
	model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
	return model

model = KerasClassifier(build_fn=lenetModel, verbose=0)
# define the grid search parameters
batch_size = [10, 20,50,100,256,512]
epochs = [10,20,50,100]
param_grid = dict(batch_size=batch_size, nb_epoch=epochs)
grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=8)
grid_result = grid.fit(trainX,trainY)
# summarize results
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
for params, mean_score, scores in grid_result.grid_scores_:
    print("%f (%f) with: %r" % (scores.mean(), scores.std(), params))